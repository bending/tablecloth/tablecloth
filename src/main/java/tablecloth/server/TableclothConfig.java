package tablecloth.server;

import com.google.common.base.Throwables;
import net.minecraft.server.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import tablecloth.server.command.InfoCommand;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class TableclothConfig {

    private static final String HEADER = "This is the main configuration file for Tablecloth.\n" +
            "\nWARN: ENABLE \'Performance.fasterStartup\' ONLY IF YOU CREATED AND GENERATED AND GENERATED ALL WORLDS.";
    public static int entityCollideFrequency = 2;
    public static int maxEntityCollisionsPerTick = 2;
    public static Boolean silentJoin;
    public static Boolean silentLeft;
    public static String nonPermissions;
    public static Boolean silentPermissions;
    public static String unknownCommandMessage;
    public static Boolean silentUnknownCommandMessage;
    public static String serverStartup;


    public static Boolean fasterStartup;
    public static Boolean preventCreationCrashChunks;

    /*========================================================================*/
    public static YamlConfiguration config;
    /*========================================================================*/
    static Map<String, Command> commands;
    public static File CONFIG_FILE;

    public static void init(File configFile) {
        CONFIG_FILE = configFile;
        config = new YamlConfiguration();
        try {
            config.load(CONFIG_FILE);
        } catch (IOException ex) {
        } catch (InvalidConfigurationException ex) {
            Bukkit.getLogger().log(Level.SEVERE, "Could not load tablecloth.yml, please correct your syntax errors", ex);
            throw Throwables.propagate(ex);
        }
        config.options().header(HEADER);
        config.options().copyDefaults(true);

        commands = new HashMap<>();
        commands.put("info", new InfoCommand());

        entityCollideFrequency = getInt("Performance.entityCollideFrequency", entityCollideFrequency);
        maxEntityCollisionsPerTick = getInt("Performance.maxEntityCollisionsPerTick", maxEntityCollisionsPerTick);
        silentJoin = getBoolean("Messages.Disable.PlayerJoinMessage", true);
        silentLeft = getBoolean("Messages.Disable.PlayerQuitMessage", true);
        silentPermissions = getBoolean("Messages.Disable.Permission", true);
        nonPermissions = transform(getString("Messages.Permission", "&4You do not have permission!"));
        unknownCommandMessage = transform(getString("Messages.Unknown-Command", "Unknown command. Type \"/help\" for help."));
        silentUnknownCommandMessage = getBoolean("Messages.Disable.Unknown-Command", true);
        serverStartup = getString("Messages.Startup", "Server is still starting on Spigot+Forge 1.12.2 by CKATEPTb!");

        preventCreationCrashChunks = getBoolean("Performance.preventCreationCrashChunks", false);
        fasterStartup = getBoolean("Performance.fasterStartup", false);

        readConfig(TableclothConfig.class, null);
    }

    private static String transform(String s) {
        return ChatColor.translateAlternateColorCodes('&', s).replaceAll("\\\\n", "\n");
    }

    public static void registerCommands() {
        for (Map.Entry<String, Command> entry : commands.entrySet()) {
            MinecraftServer.getServerInst().server.getCommandMap().register(entry.getKey(), "info", entry.getValue());
        }
    }

    static void readConfig(Class<?> clazz, Object instance) {
        for (Method method : clazz.getDeclaredMethods()) {
            if (Modifier.isPrivate(method.getModifiers())) {
                if (method.getParameterTypes().length == 0 && method.getReturnType() == Void.TYPE) {
                    try {
                        method.setAccessible(true);
                        method.invoke(instance);
                    } catch (InvocationTargetException ex) {
                        throw Throwables.propagate(ex.getCause());
                    } catch (Exception ex) {
                        Bukkit.getLogger().log(Level.SEVERE, "Error invoking " + method, ex);
                    }
                }
            }
        }

        try {
            config.save(CONFIG_FILE);
        } catch (IOException ex) {
            Bukkit.getLogger().log(Level.SEVERE, "Could not save " + CONFIG_FILE, ex);
        }
    }


    private static void set(String path, Object val) {
        config.set(path, val);
    }

    private static boolean getBoolean(String path, boolean def) {
        config.addDefault(path, def);
        return config.getBoolean(path, config.getBoolean(path));
    }

    public static <T> T getValue(String path, Class<T> valueType, T def) {
        Object val = config.get(path);
        if (valueType.isInstance(val)) {
            return (T) val;
        }
        config.set(path, def);
        return def;
    }

    private static double getDouble(String path, double def) {
        config.addDefault(path, def);
        return config.getDouble(path, config.getDouble(path));
    }

    private static float getFloat(String path, float def) {
        // TODO: Figure out why getFloat() always returns the default value.
        return (float) getDouble(path, def);
    }

    private static int getInt(String path, int def) {
        config.addDefault(path, def);
        return config.getInt(path, config.getInt(path));
    }

    private static <T> List getList(String path, T def) {
        config.addDefault(path, def);
        return config.getList(path, config.getList(path));
    }

    private static String getString(String path, String def) {
        config.addDefault(path, def);
        return config.getString(path, config.getString(path));
    }

    public static void reload() {
        try {
            config.save(CONFIG_FILE);
            config.load(CONFIG_FILE);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
}
