package net.minecraftforge.cauldron;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.ChunkProviderServer;

import java.util.HashMap;
import java.util.Map;

public class CauldronHooks {

    private static Map<Class<? extends Entity>, EntityCache> entityCache = new HashMap<>();

    /*
     * Thermos
     * 0 = false, dependent on others
     * 1 = true
     * -1 = false, absolute
     */
    public static byte canEntityTick(Entity entity, World world) {
        if (entity == null) {
            return 0;
        }

        int cX = MathHelper.floor(entity.posX) >> 4, cZ = MathHelper.floor(entity.posZ) >> 4;
        EntityCache seCache = entityCache.get(entity.getClass());
        if (seCache == null) {
            String seConfigPath = entity.getClass().getName().replace(".", "-");
            seConfigPath = seConfigPath.replaceAll("[^A-Za-z0-9\\-]", ""); // Fix up odd class names to prevent YAML errors
            seCache = new EntityCache(entity.getClass(), world.getWorldInfo().getWorldName().toLowerCase(), seConfigPath, false, false, 1);
            entityCache.put(entity.getClass(), seCache);
        }

        if (seCache.neverEverTick) {
            return -1;
        }

        // Skip tick interval
        if (seCache.tickInterval > 0 && (world.getWorldInfo().getWorldTotalTime() % seCache.tickInterval == 0L)) {
            return 0;
        }

        // Tick with no players near?
        if (seCache.tickNoPlayers) {
            return 1;
        }

        if (world.getChunkProvider() instanceof ChunkProviderServer) // Thermos - allow the server to tick entities that are in chunks trying to unload
        {
            ChunkProviderServer cps = ((ChunkProviderServer) world.getChunkProvider());
            if (cps.droppedChunksSet.contains(ChunkPos.asLong(cX, cZ))) {
                Chunk c = cps.getChunkIfLoaded(cX, cZ);
                if (c.lastAccessedTick < 2L) {
                    return 1;
                }
            }
        }

        return -1;
    }
}
